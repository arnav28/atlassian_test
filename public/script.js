(function(){

// Store data by each track
var data = {};

// load and render 
initialize();

function initialize() {
  // Get the website data from json file
  var obj = loadJSON();

  if(obj){
      // Sort the items by track title
    obj.Items.sort(function(a, b){
                var obj1 = a.Track.Title;
                var obj2 = b.Track.Title;
                if (obj1>obj2) return 1;
                else if (obj1<obj2) return -1;
                else return 0;
            });

    // Create Main Navigation Tabs
    createMainNav(obj.Items);
  }
}

// Load json data from file synchronously
function loadJSON(callback) {   
  var results;
  $.ajax({
    dataType: "json", 
    async: false, 
    url: "sessions.json",
    success: function(response) {
        results = response;
    }
  });
  return results;
}

function createMainNav(obj){
  // Curent tab 
  var currentTab;

  // HTML for naviagation
  var tabsHTML= "";
  for(var i = 0; i < obj.length; i++){
    if(currentTab != obj[i].Track.Title){
      // Render current tab
      renderMainNavTab(obj[i]);
      // Set the current tab state
      currentTab = obj[i].Track.Title;
      // Update storage object with Title as key
      data[obj[i].Track.Title] = [];
    }
    // Add to storage 
    data[obj[i].Track.Title].push(obj[i]);
  }

  // Open first tab from the main naviagation
  activateTab("mainTabs");
}

// Create Tab node and append to top naviagation 
function renderMainNavTab(item){
    // Create li node and add listener for click event
    var liEle = document.createElement("LI");
    liEle.id = item.Track.Title;
    liEle.addEventListener('click', openTab, false);

    // Add innerHTML
    var html = "<a href='javascript:void(0);'>"+ item.Track.Title;  +"</a>";
    liEle.innerHTML = html;

    // Append to ul 
    document.getElementById("mainTabs").appendChild(liEle);
}

function openTab(){
  // Update selected tab state
  $(".topNav .topActive").removeClass("topActive");
  $(this).addClass("topActive");

  // Clear previous html
  document.getElementById("sideTabs").innerHTML = "";
  document.getElementById("content").innerHTML = "";

  // Add Naivattion Title 
  document.getElementById("navTitle").innerHTML = this.id;

  // Create and render 
  initContent(data[this.id]);
}

function initContent(items){
  var sideHTML = "";
  // Loop through items and create side navigation and content
  for(var i = 0; i < items.length; i++){
    renderSideNavTab(items[i]);
  }

  // Open first tab from the side naviagation
  activateTab("sideTabs");
}

// Create Tab node and append to top naviagation 
function renderSideNavTab(item){

    // Create li node and add listener for click event
    var liEle = document.createElement("LI");

    // Use data-attribute for storing id
    liEle.dataset.id = item.Id

    // Add click for toggle side nav state
    liEle.addEventListener('click', toggleSideNav, false);

    // Add innerHTML
    var html = "<a href='javascript:void(0);'>"+ item.Title  +"</a><cite>" + getAuthor(item) + "</cite>";
    liEle.innerHTML = html;

    // Append to ul 
    document.getElementById("sideTabs").appendChild(liEle);

    // Create nodes for each item Description
    renderDetails(item);
}

function toggleSideNav(){
  // Get data attribute value
  var id = this.dataset.id;

  // Update selected tab state
  $(".sideNav .sideActive").removeClass("sideActive");
  $(this).addClass("sideActive");

  // Toggle content div
  $(".tab-open").addClass("tab-hidden").removeClass("tab-open");
  document.getElementById(id).className = "tab-open";
}

function renderDetails(item){
    // Create html construct for details and append it to  div
    var detailsEle = document.createElement("div");
    detailsEle.id = item.Id;
    detailsEle.className = "tab-hidden";
    document.getElementById("content").appendChild(detailsEle);

    var html = "<h2>" + item.Title + "</h2>" +
                "<cite>" + getAuthor(item) + "</cite>" + 
                "<p>" + item.Description + "</p>";

    document.getElementById(detailsEle.id).innerHTML = html;
}

// Return speaker string
function getAuthor(item){
  var speakerArray = [];
  var string = "";
  // Check if Speakers exists
  if(item.Speakers){
    for(var i = 0; i < item.Speakers.length; i++){
      // Create String
      speakerArray.push(item.Speakers[i].FirstName + "\u00A0" +item.Speakers[i].LastName);
    }
    string = speakerArray.toString() + "\u003A \u00A0" +item.Speakers[0].Company;
  }
  return string;
}

// Fire click event for first tab
function activateTab(ele){
    document.getElementById(ele).firstChild.click();
}

})();