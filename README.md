# README #


### What is this repository for? ###

* Interview assignment for Atlassian web developer position

### Running the project ###

* Method 1: https://fir-683b2.firebaseapp.com/ - hosted on firebase without minification
* Method 2: cd project/public and run http-server (https://www.npmjs.com/package/http-server)

### Technologies ###

* HTML,CSS,JavaScript,jQuery